<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../general/bootstrapJs.jsp" %>
<link href="${pageContext.request.contextPath}/styles.css" rel="stylesheet" type="text/css">

<html>
<head>
    <title>Marks changing</title>
</head>
<body>
<div class="jumbotron">
    <h3>Mark change form</h3>
</div>
<form action="/onlineCourses/marks/changeMark" method="GET">
    <table class="center">
        <tr>
            <td>
                <div class="form-group">
                    <label>Comment</label>
                    <input type="text" class="form-control" pattern="[\w*\s,]*" title="Only letters and commas"
                           name="comment" placeholder="Enter comment">
                    <small class="form-text text-muted">(max 30 characters a-z and A-Z)</small>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label>Mark</label>
                    <input type="text" class="form-control" pattern="[\d+]*" title="Only digits"
                           name="mark" placeholder="Enter mark">
                </div>
            </td>
        </tr>
        <tr>
            <input type="hidden" name="studentId"
                   value="${param.studentId}"/>
        </tr>
        <tr>
            <input type="hidden" name="taskId"
                   value="${param.taskId}"/>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" class="btn btn-secondary" value="Submit">
                <input type="reset" class="btn btn-secondary" value="Clear fields">
            </td>
        </tr>
    </table>
</form>
<div class="alert alert-secondary" role="alert" align="center">
    Back to main page <a href='/onlineCourses/courses/menu' class="alert-link">click here</a>
</div>
</body>
</html>