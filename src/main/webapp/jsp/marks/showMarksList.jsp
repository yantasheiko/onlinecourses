<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="../general/bootstrapJs.jsp" %>

<fmt:setLocale value="en_US" scope="session"/>
<fmt:setBundle basename="marks"/>

<html>
<head>
    <title>Marks list</title>
</head>
<body>
<div class="jumbotron">
    <h2 class="display-4">Marks</h2>
</div>

<table class="table table-dark">
    <thead>
    <tr>
        <th><fmt:message key="mark.nameOfTask"/></th>
        <th><fmt:message key="mark.comment"/></th>
        <th><fmt:message key="mark.mark"/></th>
        <th><fmt:message key="student.name"/></th>
        <th><fmt:message key="student.surname"/></th>
    </tr>
    </thead>
    <c:forEach items="${requestScope.entitiesList}" var="mark">
        <tr>
        <td><c:out value="${mark.getNameOfTask()}"/></td>
        <td><c:out value="${mark.getComment()}"/></td>
        <td><c:out value="${mark.getMark()}"/></td>
        <td><c:out value="${mark.getStudentName()}"/></td>
        <td><c:out value="${mark.getStudentSurname()}"/></td>
        <c:choose>
            <c:when test="${'teacher'.equalsIgnoreCase(sessionScope.user.getType().name())}">
                <form action='delete' method='GET'>
                    <td>
                        <button type="submit" class="btn btn-secondary" name="deleteMarkId"
                                value="${mark.getMarkId()}">Delete
                        </button>
                    </td>
                </form>
                </tr>
                <form action="update" method='GET'>
                    <tr>
                        <td>
                            <div class="form-group">
                                <input type="text" name="comment" pattern="[\w*\s,]*" title="Only letters and commas"
                                       class="form-control"
                                       placeholder="comment">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text" name="mark" pattern="[\d+]*" title="Only digits"
                                       class="form-control"
                                       placeholder="mark">
                            </div>
                        </td>
                        <td><input type="hidden" name="updateMarkId" value="${mark.getMarkId()}">
                            <input type="submit" class="btn btn-secondary" value="Update"></td>
                        <td><input type="reset" class="btn btn-secondary" value="Clear fields"></td>
                    </tr>
                </form>
            </c:when>
            <c:otherwise>
                </tr>
            </c:otherwise>
        </c:choose>
    </c:forEach>
</table>
<div class="alert alert-secondary" role="alert">
    Back to main page <a href='/onlineCourses/courses/menu' class="alert-link">click here</a>
</div>
</body>
</html>