<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="../general/bootstrapJs.jsp" %>

<fmt:setLocale value="en_US" scope="session"/>
<fmt:setBundle basename="users"/>

<html>
<head>
    <title>List of users</title>
</head>
<body>
<div class="jumbotron">
    <h2 class="display-4">List of users</h2>
</div>

<table class="table table-dark">
    <thead>
    <tr>
        <th><fmt:message key="user.email"/></th>
        <th><fmt:message key="user.name"/></th>
        <th><fmt:message key="user.surname"/></th>
        <th><fmt:message key="user.address"/></th>
        <th><fmt:message key="user.phoneNumber"/></th>
        <th><fmt:message key="user.type"/></th>
        <th><fmt:message key="user.specialization"/></th>
    </tr>
    </thead>
    <c:forEach items="${requestScope.users}" var="user">
        <c:choose>
            <c:when test="${sessionScope.user.isAdmin()}">
                <tr>
                    <td><c:out value="${user.getEmail()}"/></td>
                    <td><c:out value="${user.getName()}"/></td>
                    <td><c:out value="${user.getSurname()}"/></td>
                    <td><c:out value="${user.getAddress()}"/></td>
                    <td><c:out value="${user.getPhoneNum()}"/></td>
                    <td><c:out value="${user.getType()}"/></td>
                    <c:choose>
                        <c:when test="${user.getSpecialization() != null}">
                            <td><c:out value="${user.getSpecialization()}"/></td>
                        </c:when>
                        <c:otherwise>
                            <td>not a teacher</td>
                        </c:otherwise>
                    </c:choose>
                    <form action="delete" method="GET">
                        <td>
                            <button type="submit" class="btn btn-secondary" name="deleteUserId"
                                    value="${user.getUserId()}">Delete
                            </button>
                        </td>
                    </form>
                </tr>
                <form action="update" method="GET">
                    <tr>
                        <td>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control"
                                       placeholder="email">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text" name="name" pattern="[\w*\s]*" title="Only letters"
                                       class="form-control"
                                       placeholder="name">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text" name="surname" pattern="[\w*\s]*" title="Only letters"
                                       class="form-control"
                                       placeholder="surname">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text" name="address" pattern="[\w*\s,]*" title="Only letters and commas"
                                       class="form-control"
                                       placeholder="address">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text" name="phoneNum"
                                       pattern='\s{0,}\+{1,1}375\s{0,}\({0,1}(([2]{1}([5]{1}|[9]{1}))|([3]{1}[3]{1})|([4]{1}[4]{1}))\)\s{0,}[0-9]{3,3}\s{0,}[0-9]{4,4}'
                                       title="enter number in this format: +375(29)7001010"
                                       class="form-control"
                                       placeholder="phone number">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <select name="userType" class="form-control">
                                    <option>TEACHER</option>
                                    <option>STUDENT</option>
                                </select>
                            </div>
                        </td>
                        <td><input type="hidden" name="updateUserId" value="${user.getUserId()}">
                            <c:if test="${user.getType().toString().equals('TEACHER')}">
                            <div class="form-group"><input type="text" name="specialization" pattern="[\w*\s,]*"
                                                           title="Only letters and commas"
                                                           class="form-control"></div>
                        </td>
                        </c:if>
                        <td><input type="submit" class="btn btn-secondary" value="Update"></td>
                        <td><input type="reset" class="btn btn-secondary" value="Clear fields"></td>
                    </tr>
                </form>
            </c:when>
        </c:choose>
    </c:forEach>
</table>
<div class="alert alert-secondary" role="alert">
    Back to main page <a href='/onlineCourses/courses/menu' class="alert-link">click here</a>
</div>
</body>
</html>