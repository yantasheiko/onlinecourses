<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../general/bootstrapJs.jsp" %>
<link href="${pageContext.request.contextPath}/styles.css" rel="stylesheet" type="text/css">

<html>
<head>
    <title>Registration result</title>
</head>
<body>
<div>
    <c:choose>
        <c:when test="${requestScope.registerUser}">
            <h1>Thanks for Registering with us</h1>
            <div class="alert alert-secondary" role="alert">
                To login <a href="/onlineCourses">Click here</a>
            </div>
        </c:when>
        <c:otherwise>
            <h1>Registration Failed</h1>
            <div class="alert alert-secondary" role="alert">
                To try again <a href="/onlineCourses/registration">Click here</a>
            </div>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>