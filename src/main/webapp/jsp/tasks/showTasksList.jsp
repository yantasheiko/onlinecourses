<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="../general/bootstrapJs.jsp" %>

<fmt:setLocale value="en_US" scope="session"/>
<fmt:setBundle basename="tasks"/>

<html>
<head>
    <title>List of tasks</title>
</head>
<body>
<div class="jumbotron">
    <h2 class="display-4">List of tasks</h2>
</div>

<table class="table table-dark">
    <thead>
    <tr>
        <th><fmt:message key="task.nameOfTask"/></th>
        <th><fmt:message key="task.description"/></th>
    </tr>
    </thead>
    <c:forEach items="${requestScope.tasks}" var="task">
        <tr>
        <td><c:out value="${task.getNameOfTask()}"/></td>
        <td><c:out value="${task.getDescription()}"/></td>
        <c:choose>
            <c:when test="${sessionScope.user.isAdmin()}">
                <form action="delete" method="GET">
                    <td>
                        <button type="submit" class="btn btn-secondary" name="deleteTaskId"
                                value="${task.getTaskId()}">Delete
                        </button>
                    </td>
                </form>
                </tr>
                <form action="update" method="GET">
                    <tr>
                        <td>
                            <div class="form-group">
                                <input type="text" name="nameOfTask" pattern="[\w*\s]*" title="Only letters"
                                       class="form-control"
                                       placeholder="name of task">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text" name="description" pattern="[\w*\s,]*"
                                       title="Only letters and commas" class="form-control"
                                       placeholder="description">
                            </div>
                        </td>
                        <td><input type="hidden" name='updateTaskId' value="${task.getTaskId()}">
                            <input type="submit" class="btn btn-secondary" value="Update"></td>
                        <td><input type="reset" class="btn btn-secondary" value="Clear fields"></td>
                    </tr>
                </form>
            </c:when>
            <c:when test="${'teacher'.equalsIgnoreCase(sessionScope.user.getType().name()) && (param.studentId != null)}">
                <form action='/onlineCourses/courses/createMarks' method="GET">
                    <td><input type="hidden" name="studentId" value="${param.studentId}"/>
                        <input type="hidden" name="taskId" value="${task.getTaskId()}"/>
                        <input type="submit" class="btn btn-secondary" value="Choose Task"></td>
                </form>
                </tr>
            </c:when>
            <c:otherwise>
                </tr>
            </c:otherwise>
        </c:choose>
    </c:forEach>
</table>
<div class="alert alert-success" role="alert">
    All completed tasks you must send to the teacher's email. You can find it on the <a
        href='/onlineCourses/teachers/teacher' class="alert-link">teachers page</a>.
</div>
<div class="alert alert-secondary" role="alert">
    Back to main page <a href='/onlineCourses/courses/menu' class="alert-link">click here</a>
</div>
</body>
</html>