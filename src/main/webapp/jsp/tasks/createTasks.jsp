<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../general/bootstrapJs.jsp" %>
<link href="${pageContext.request.contextPath}/styles.css" rel="stylesheet" type="text/css">

<html>
<head>
    <title>Tasks changing</title>
</head>
<body>
<div class="jumbotron" align="center">
    <h3>Course change form</h3>
</div>
<form action="/onlineCourses/tasks/changeTask" method="GET">
    <table class="center">
        <tr>
            <td>
                <div class="form-group">
                    <label>Name of task</label>
                    <input type="text" class="form-control" pattern="[\w*\s]*" title="Only letters"
                           name="nameOfTask" placeholder="Enter name of task">
                    <small class="form-text text-muted">(max 30 characters a-z and A-Z)</small>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label>Description</label>
                    <input type="text" class="form-control" pattern="[\w*\s,]*" title="Only letters and commas"
                           name="description" placeholder="Enter description">
                    <small class="form-text text-muted">(max 30 characters a-z and A-Z)</small>
                </div>
            </td>
        </tr>
        <tr>
            <input type="hidden" name="courseId"
                   value="${param.courseId}"/>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" class="btn btn-secondary" value="Submit">
                <input type="reset" class="btn btn-secondary" value="Clear fields">
            </td>
        </tr>
    </table>
</form>
<div class="alert alert-secondary" role="alert" align="center">
    Back to main page <a href='/onlineCourses/courses/menu' class="alert-link">click here</a>
</div>
</body>
</html>