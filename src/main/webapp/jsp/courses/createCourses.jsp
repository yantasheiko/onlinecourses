<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../general/bootstrapJs.jsp" %>
<link href="${pageContext.request.contextPath}/styles.css" rel="stylesheet" type="text/css">

<html>
<head>
    <title>Courses changing</title>
</head>
<body>
<div class="jumbotron">
    <h3>Course change form</h3>
</div>
<form action="changeCourse" method="GET">
    <table class="center">
        <tr>
            <td>
                <div class="form-group">
                    <label>Course type</label>
                    <input type="text" class="form-control" name="type" pattern="[\w*\s,]*"
                           title="Only letters and commas" placeholder="Enter course type">
                    <small class="form-text text-muted">(max 30 characters a-z and A-Z)</small>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label>Course title</label>
                    <input type="text" class="form-control" pattern="[\w*\s,]*" title="Only letters and commas"
                           name="title" placeholder="Enter course title">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label>Number of hours</label>
                    <input type="text" class="form-control" pattern="[\d+]*" title="Only digits"
                           name="numOfHours" placeholder="Enter number of hours">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label>Description</label>
                    <input type="text" class="form-control" pattern="[\w*\s,]*" title="Only letters and commas"
                           name="description" placeholder="Enter description">
                    <small class="form-text text-muted">(max 250 characters)</small>
                </div>
            </td>
        </tr>
        <tr>
            <input type="hidden" name="teacherId"
                   value="${param.teacherId}"/>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" class="btn btn-secondary" value="Submit">
                <input type="reset" class="btn btn-secondary" value="Clear fields">
            </td>
        </tr>
    </table>
</form>
<div class="alert alert-secondary" role="alert" align="center">
    Back to main page <a href='/onlineCourses/courses/menu' class="alert-link">click here</a>
</div>
</body>
</html>