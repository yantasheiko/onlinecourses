<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="bootstrapJs.jsp" %>

<html>
<head>
    <title>Deletion list</title>
</head>
<body>
<div class="jumbotron">
    <h2 class="display-4">Deletion list</h2>
</div>
<c:choose>
    <c:when test="${requestScope.courseDel != null}">
        <div class="alert alert-success" role="alert">
            <li>Course at number ${requestScope.courseDel} was deleted from DB</li>
        </div>
    </c:when>
    <c:when test="${requestScope.taskDel != null}">
        <div class="alert alert-success" role="alert">
            <li>Task at number ${requestScope.taskDel} was deleted from DB</li>
        </div>
    </c:when>
    <c:when test="${requestScope.markDel != null}">
        <div class="alert alert-success" role="alert">
            <li>Mark at number ${requestScope.markDel} was deleted from DB</li>
        </div>
    </c:when>
    <c:when test="${requestScope.userDel != null}">
        <div class="alert alert-success" role="alert">
            <li>User at number ${requestScope.userDel} was deleted from DB</li>
        </div>
    </c:when>
</c:choose>
<div class="alert alert-secondary" role="alert">
    Back to main page <a href='/onlineCourses/courses/menu' class="alert-link">click here</a>
</div>
</body>
</html>