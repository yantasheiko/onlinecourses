<%@ include file="../general/bootstrapJs.jsp" %>
<link href="${pageContext.request.contextPath}/styles.css" rel="stylesheet" type="text/css">

<html>
<head>
    <title>Wrong resource error</title>
</head>
<body>
<div class="alert alert-success" role="alert">
    ${requestScope.resourceError}
</div>
<div class="alert alert-secondary" role="alert">
    Back to main page <a href='/onlineCourses/courses/menu' class="alert-link">click here</a>
</div>
</body>
</html>