<%@ include file="../general/bootstrapJs.jsp" %>
<link href="${pageContext.request.contextPath}/styles.css" rel="stylesheet" type="text/css">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Error Page</title>
</head>
<body>
<div>
    <h1>Your Login Was Unsuccessful - Please Try Again</h1>
    <div class="alert alert-warning" role="alert">
        To login again <a href="/onlineCourses" class="alert-link">click here</a>
    </div>
</div>
</body>
</html>