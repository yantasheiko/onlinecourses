package courses.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class CourseDto {

    private Integer courseId;
    private String type;
    private String title;
    private Integer numOfHours;
    private String description;

    private String name;
    private String surname;
    private Integer userId;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getNumOfHours() {
        return numOfHours;
    }

    public void setNumOfHours(Integer numOfHours) {
        this.numOfHours = numOfHours;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public boolean equals(final Object obj) {
        if (obj instanceof CourseDto) {
            final CourseDto other = (CourseDto) obj;
            return new EqualsBuilder()
                    .append(courseId, other.courseId)
                    .append(type, other.type)
                    .append(title, other.title)
                    .append(numOfHours, other.numOfHours)
                    .append(description, other.description)
                    .append(name, other.name)
                    .append(surname, other.surname)
                    .append(userId, other.userId)
                    .isEquals();
        }
        return false;
    }

    public int hashCode() {
        return new HashCodeBuilder()
                .append(courseId)
                .append(type)
                .append(title)
                .append(numOfHours)
                .append(description)
                .append(name)
                .append(surname)
                .append(userId)
                .toHashCode();
    }

    public String toString() {
        return new ToStringBuilder(this)
                .append("courseId", courseId)
                .append("type", type)
                .append("title", title)
                .append("numOfHours", numOfHours)
                .append("description", description)
                .append("name", name)
                .append("surname", surname)
                .append("userId", userId)
                .toString();
    }
}
