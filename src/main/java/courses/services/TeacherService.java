package courses.services;

import courses.dao.impl.TeacherDaoImpl;
import courses.entities.Teacher;

import java.util.List;

public class TeacherService {

    private TeacherDaoImpl dao;

    public TeacherService() {
        dao = new TeacherDaoImpl();
    }

    public Teacher findById(Integer id) {
        return dao.findById(id);
    }

    public List<Teacher> findAll() {
        return dao.findAll();
    }

}
