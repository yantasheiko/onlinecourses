package courses.services;

import static java.lang.Integer.parseInt;

import courses.dao.impl.TaskDaoImpl;
import courses.entities.Course;
import courses.entities.Task;

import java.util.List;

public class TaskService {

    private TaskDaoImpl dao;
    private CourseService courseService;

    public TaskService() {
        dao = new TaskDaoImpl();
        courseService = new CourseService();
    }

    public void update(Task entity) {
        dao.update(entity);
    }

    public Task findById(Integer id) {
        return dao.findById(id);
    }

    public void delete(Task entity) {
        dao.delete(entity);
    }

    public List<Task> findAll() {
        return dao.findAll();
    }

    public boolean isExists(Task entity) {
        return dao.isExists(entity);
    }

    public void add(Task entity) {
        dao.add(entity);
    }

    public boolean create(Task task) {
        boolean result = !isExists(task);

        if (result) {
            add(task);
        }
        return result;
    }

    public Integer deleteTask(String taskStr) {
        Integer taskId = null;
        if (taskStr != null) {
            taskId = parseInt(taskStr);
            Task task = findById(taskId);
            delete(task);
        }
        return taskId;
    }

    public Task addRelationships(Task task, int courseId) {
            Course course = courseService.findById(courseId);
            task.setCourse(course);
            return task;
    }

    public void updateTask (Task task, int taskId) {
        Task tk = findById(taskId);
        task.setTaskId(taskId);
        task.setCourse(tk.getCourse());
    }

}
