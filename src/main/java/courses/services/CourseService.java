package courses.services;

import static courses.converters.CourseConverter.toCourseDto;

import static java.lang.Integer.parseInt;

import courses.dao.impl.CourseDaoImpl;
import courses.entities.Course;
import courses.entities.Teacher;
import courses.model.CourseDto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class CourseService {

    private CourseDaoImpl dao;
    private TeacherService teacherService;

    public CourseService() {
        dao = new CourseDaoImpl();
        teacherService = new TeacherService();
    }

    public void update(Course entity) {
        dao.update(entity);
    }

    public Course findById(Integer id) {
        return dao.findById(id);
    }

    public void delete(Course entity) {
        dao.delete(entity);
    }

    public List<Course> findAll() {
        return dao.findAll();
    }

    public boolean isExists(Course entity) {
        return dao.isExists(entity);
    }

    public void add(Course entity) {
        dao.add(entity);
    }

    public Integer deleteCourse(String courseStr) {
        Integer courseId = null;
        if (courseStr != null) {
            courseId = parseInt(courseStr);
            Course course = findById(courseId);
            delete(course);
        }
        return courseId;
    }

    public Course addRelationships(Course course, Integer teacherId) {
        Teacher teacher = teacherService.findById(teacherId);
        if (course.getTeachers() == null) {
            course.setTeachers(new HashSet<>());
        }
        course.getTeachers().add(teacher);
        return course;
    }

    public void addCourseId(Course course, Integer courseId) {
        course.setCourseId(courseId);
    }

    public boolean create(Course course) {
        boolean result = !isExists(course);

        if (result) {
            add(course);
        }
        return result;
    }


    public List<CourseDto> getCoursesDto() {
        List<CourseDto> list = new ArrayList<>();

        for (Course course : findAll()) {
            CourseDto courseDto = toCourseDto(course, getRelatedTeacher(course));
            list.add(courseDto);
        }

        return list;
    }

    private Teacher getRelatedTeacher(Course course) {
        return teacherService.findById(course.getTeacherId(course));
    }
}
