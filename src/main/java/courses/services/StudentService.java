package courses.services;

import courses.dao.impl.StudentDaoImpl;
import courses.entities.Student;

import java.util.List;

public class StudentService {

    private StudentDaoImpl dao;

    public StudentService() {
        dao = new StudentDaoImpl();
    }

    public Student findById(Integer id) {
        return dao.findById(id);
    }

    public List<Student> findAll() {
        return dao.findAll();
    }

}
