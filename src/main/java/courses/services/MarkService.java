package courses.services;

import static courses.converters.MarkConverter.toMarkDto;

import static java.lang.Integer.parseInt;

import courses.dao.impl.MarkDaoImpl;
import courses.entities.Mark;
import courses.entities.Student;
import courses.entities.Task;
import courses.model.MarkDto;

import java.util.ArrayList;
import java.util.List;

public class MarkService {

    private MarkDaoImpl dao;
    private StudentService studentService;
    private TaskService taskService;

    public MarkService() {
        dao = new MarkDaoImpl();
        studentService = new StudentService();
        taskService = new TaskService();
    }

    public void update(Mark entity) {
        dao.update(entity);
    }

    public Mark findById(Integer id) {
        return dao.findById(id);
    }

    public void delete(Mark entity) {
        dao.delete(entity);
    }

    public List<Mark> findAll() {
        return dao.findAll();
    }

    public boolean isExists(Mark entity) {
        return dao.isExists(entity);
    }

    public void add(Mark entity) {
        dao.add(entity);
    }

    public Integer deleteMark(String markStr) {
        Integer markId = null;
        if (markStr != null) {
            markId = parseInt(markStr);
            Mark mark = findById(markId);
            delete(mark);
        }
        return markId;
    }

    public boolean create(Mark mark) {
        boolean result = !isExists(mark);

        if (result) {
            add(mark);
        }
        return result;
    }

    public Mark addRelationships(Mark mark, int studentId, int taskId) {
            Student student = studentService.findById(studentId);
            Task task = taskService.findById(taskId);
            mark.setStudent(student);
            mark.setTask(task);
            return mark;
    }

    public void updateMark(Mark mark, int markId) {
            Mark fb = findById(markId);
            mark.setMarkId(markId);
            mark.setStudent(fb.getStudent());
            mark.setTask(fb.getTask());
    }

    public List<MarkDto> getMarksDto() {
        List<MarkDto> list = new ArrayList<>();

        for (Mark mark : findAll()) {
            MarkDto markDto = toMarkDto(mark, getRelatedStudent(mark), getRelatedTask(mark));
            list.add(markDto);
        }

        return list;
    }

    private Student getRelatedStudent(Mark mark) {
        return studentService.findById(mark.getStudent().getUserId());
    }

    private Task getRelatedTask(Mark mark) {
        return taskService.findById(mark.getTask().getTaskId());
    }

}
