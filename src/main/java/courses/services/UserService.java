package courses.services;

import static courses.converters.UserConverter.toUserDto;

import static java.lang.Integer.parseInt;

import courses.entities.Teacher;
import courses.entities.User;
import courses.dao.impl.UserDaoImpl;
import courses.model.UserDto;

import java.util.ArrayList;
import java.util.List;

public class UserService {

    private UserDaoImpl dao;
    private TeacherService teacherService;

    public UserService() {
        dao = new UserDaoImpl();
        teacherService = new TeacherService();
    }

    public void update(User entity) {
        dao.update(entity);
    }

    public User findById(Integer id) {
        return dao.findById(id);
    }

    public void delete(User entity) {
        dao.delete(entity);
    }

    public List<User> findAll() {
        return dao.findAll();
    }

    public boolean isExists(User entity) {
        return dao.isExists(entity);
    }

    public void add(User entity) {
        dao.add(entity);
    }

    public boolean authenticateUser(String email, String password) {
        User user = getUserByEmail(email);
        return (user != null) && user.getEmail().equals(email) && user.getPassword().equals(password);
    }

    public User getUserByEmail(String email) {
        return dao.getUserByEmail(email, dao.getCurrentSession());
    }

    public boolean create(User user) {
        boolean result = !isExists(user);

        if (result) {
            add(user);
        }
        return result;
    }

    public Integer deleteUser(String userStr) {
        Integer userId = null;
        if (userStr != null) {
            userId = parseInt(userStr);
            User user = findById(userId);
            delete(user);
        }
        return userId;
    }

    public List<UserDto> getUsersDto() {
        List<UserDto> list = new ArrayList<>();

        for (User user : findAll()) {
            UserDto userDto = toUserDto(user, getRelatedTeacher(user));
            list.add(userDto);
        }

        return list;
    }

    private Teacher getRelatedTeacher(User user) {
        return teacherService.findById(user.getUserId());
    }

    public User setPassAndId(User user, int userId) {//return User
        user.setUserId(userId);
        user.setPassword(findById(userId).getPassword());
        return user;
    }
}
