package courses.web;

import static courses.dao.utils.ErrorCodeHelper.DAO_EXCEPTION_CODE;
import static courses.dao.utils.ErrorCodeHelper.IO_EXCEPTION_CODE;

import static courses.web.ServletHelper.CONTENT_TYPE;
import static courses.web.ServletHelper.SHOW_DELETED_MARK;
import static courses.web.ServletHelper.SHOW_UPDATED_MARK;
import static courses.web.ServletHelper.CHANGE_MARK_INFO;
import static courses.web.ServletHelper.SHOW_MARKS;
import static courses.web.ServletHelper.getUrl;

import static java.lang.Integer.parseInt;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

import courses.builders.MarkBuilder;
import courses.entities.Mark;
import courses.services.MarkService;
import org.apache.log4j.Logger;
import courses.dao.DAOException;
import courses.validation.Validator;
import courses.validation.ValidateException;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletConfig;

public class MarkServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(MarkServlet.class.getName());
    private MarkService markService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        markService = new MarkService();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType(CONTENT_TYPE);

        try {
            switch (getUrl(req)) {
                case SHOW_MARKS:
                    showMarksList(req, resp);
                    break;
                case SHOW_DELETED_MARK:
                    delete(req, resp);
                    break;
                case SHOW_UPDATED_MARK:
                    update(req, resp);
                    break;
                case CHANGE_MARK_INFO:
                    changeMark(req, resp);
                    break;
                default:
                    resourceError(req, resp);
                    break;
            }
        } catch (ValidateException ve) {
            logger.error(ve);
            resp.setStatus(SC_BAD_REQUEST);
            validateInfo(req, resp, ve.getMessage());
        } catch (DAOException e) {
            logger.error(e);
            resp.sendError(SC_INTERNAL_SERVER_ERROR, DAO_EXCEPTION_CODE);
        } catch (IOException ioe) {
            logger.error(ioe);
            resp.sendError(SC_INTERNAL_SERVER_ERROR, IO_EXCEPTION_CODE);
        }
    }

    private void resourceError(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("resourceError", "Resource not found");
        getServletContext().getRequestDispatcher("/jsp/errors/resourceError.jsp").forward(req, resp);
    }

    private void validateInfo(HttpServletRequest req, HttpServletResponse resp, String errorMessage) throws ServletException, IOException {
        req.setAttribute("invalidField", errorMessage);
        getServletContext().getRequestDispatcher("/jsp/errors/validateError.jsp").forward(req, resp);
    }

    private void showMarksList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, DAOException, IOException {
        req.setAttribute("entitiesList", markService.getMarksDto());
        getServletContext().getRequestDispatcher("/jsp/marks/showMarksList.jsp").forward(req, resp);
    }

    private void changeMark(HttpServletRequest req, HttpServletResponse resp) throws ServletException, DAOException, IOException, ValidateException {
        Mark mark = MarkBuilder.build(req);
        if (req.getAttribute("error") != null) {
            getServletContext().getRequestDispatcher("/jsp/errors/xssError.jsp").forward(req, resp);
        } else {
            Validator.validateMark(mark);
            int studentId = parseInt(req.getParameter("studentId"));
            int taskId = parseInt(req.getParameter("taskId"));
            if (areIdsExist(studentId, taskId)) {
                mark = markService.addRelationships(mark, studentId, taskId);
            }

            boolean result = markService.create(mark);

            req.setAttribute("changeMark", result);
            getServletContext().getRequestDispatcher("/jsp/marks/createMarkResult.jsp").forward(req, resp);
        }
    }

    private void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, DAOException, IOException {
        String markId = req.getParameter("deleteMarkId");
        int deletedMarkId = markService.deleteMark(markId);

        req.setAttribute("markDel", deletedMarkId);
        getServletContext().getRequestDispatcher("/jsp/general/delete.jsp").forward(req, resp);
    }

    private void update(HttpServletRequest req, HttpServletResponse resp) throws ServletException, DAOException, IOException, ValidateException {
        Mark mark = MarkBuilder.build(req);
        if (req.getAttribute("error") != null) {
            getServletContext().getRequestDispatcher("/jsp/errors/xssError.jsp").forward(req, resp);
        } else {
            Validator.validateMark(mark);
            int markId = parseInt(req.getParameter("updateMarkId"));
            if (markId > 0) {
                markService.updateMark(mark, markId);
            }
            markService.update(mark);


            req.setAttribute("updatedMark", mark);
            getServletContext().getRequestDispatcher("/jsp/general/update.jsp").forward(req, resp);
        }
    }

    private boolean areIdsExist(Integer studentId, Integer taskId) {
        return (studentId != null) && (taskId != null);
    }

}
