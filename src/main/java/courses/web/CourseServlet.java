package courses.web;

import static courses.dao.utils.ErrorCodeHelper.DAO_EXCEPTION_CODE;
import static courses.dao.utils.ErrorCodeHelper.IO_EXCEPTION_CODE;

import static courses.web.ServletHelper.CONTENT_TYPE;
import static courses.web.ServletHelper.SHOW_COURSES;
import static courses.web.ServletHelper.SHOW_DELETED_COURSE;
import static courses.web.ServletHelper.SHOW_UPDATED_COURSE;
import static courses.web.ServletHelper.CHANGE_COURSE_INFO;
import static courses.web.ServletHelper.SHOW_MENU;
import static courses.web.ServletHelper.getUrl;

import static java.lang.Integer.parseInt;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

import courses.builders.CourseBuilder;
import courses.entities.Course;
import courses.dao.DAOException;
import courses.services.CourseService;
import courses.validation.Validator;
import courses.validation.ValidateException;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletConfig;

import org.apache.log4j.Logger;

public class CourseServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(CourseServlet.class.getName());
    private CourseService courseService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        courseService = new CourseService();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType(CONTENT_TYPE);

        try {
            switch (getUrl(req)) {
                case SHOW_MENU:
                    showMenu(req, resp);
                    break;
                case SHOW_COURSES:
                    showCoursesList(req, resp);
                    break;
                case SHOW_DELETED_COURSE:
                    delete(req, resp);
                    break;
                case SHOW_UPDATED_COURSE:
                    update(req, resp);
                    break;
                case CHANGE_COURSE_INFO:
                    changeCourse(req, resp);
                    break;
                default:
                    resourceError(req, resp);
                    break;
            }
        } catch (ValidateException ve) {
            logger.error(ve);
            resp.setStatus(SC_BAD_REQUEST);
            validateInfo(req, resp, ve.getMessage());
        } catch (DAOException e) {
            logger.error(e);
            resp.sendError(SC_INTERNAL_SERVER_ERROR, DAO_EXCEPTION_CODE);
        } catch (IOException ioe) {
            logger.error(ioe);
            resp.sendError(SC_INTERNAL_SERVER_ERROR, IO_EXCEPTION_CODE);
        }
    }

    private void resourceError(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("resourceError", "Resource not found");
        getServletContext().getRequestDispatcher("/jsp/errors/resourceError.jsp").forward(req, resp);
    }

    private void showMenu(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/onlineCourses").forward(req, resp);
    }

    private void validateInfo(HttpServletRequest req, HttpServletResponse resp, String errorMessage) throws ServletException, IOException {
        req.setAttribute("invalidField", errorMessage);
        getServletContext().getRequestDispatcher("/jsp/errors/validateError.jsp").forward(req, resp);
    }

    private void showCoursesList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, DAOException, IOException {
        req.setAttribute("entitiesList", courseService.getCoursesDto());
        getServletContext().getRequestDispatcher("/jsp/courses/showCoursesList.jsp").forward(req, resp);
    }

    private void changeCourse(HttpServletRequest req, HttpServletResponse resp) throws ServletException, DAOException, IOException, ValidateException {
        Course course = CourseBuilder.build(req);
        if (req.getAttribute("error") != null) {
            getServletContext().getRequestDispatcher("/jsp/errors/xssError.jsp").forward(req, resp);
        } else {
            Validator.validateCourse(course);
            int teacherId = parseInt(req.getParameter("teacherId"));
            if (teacherId > 0) {
                course = courseService.addRelationships(course, teacherId);
            }
            boolean result = courseService.create(course);


            req.setAttribute("changeCourse", result);
            getServletContext().getRequestDispatcher("/jsp/courses/createCourseResult.jsp").forward(req, resp);
        }
    }

    private void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, DAOException, IOException {
        String courseId = req.getParameter("deleteCourseId");
        int deletedCourseId = courseService.deleteCourse(courseId);

        req.setAttribute("courseDel", deletedCourseId);
        getServletContext().getRequestDispatcher("/jsp/general/delete.jsp").forward(req, resp);
    }

    private void update(HttpServletRequest req, HttpServletResponse resp) throws ServletException, DAOException, IOException, ValidateException {
        Course course = CourseBuilder.build(req);

        if (req.getAttribute("error") != null) {
            getServletContext().getRequestDispatcher("/jsp/errors/xssError.jsp").forward(req, resp);
        } else {
            Validator.validateCourse(course);
            int courseId = parseInt(req.getParameter("updateCourseId"));
            int teacherId = parseInt(req.getParameter("updateTeacherId"));

            if (areIdsExist(courseId, teacherId)) {
                courseService.addCourseId(course, courseId);
                course = courseService.addRelationships(course, teacherId);
            }
            courseService.update(course);

            req.setAttribute("updatedCourse", course);
            getServletContext().getRequestDispatcher("/jsp/general/update.jsp").forward(req, resp);
        }
    }

    private boolean areIdsExist(Integer courseId, Integer teacherId) {
        return (courseId != null) && (teacherId != null);
    }
}
