package courses.web;

import static courses.dao.utils.ErrorCodeHelper.DAO_EXCEPTION_CODE;
import static courses.dao.utils.ErrorCodeHelper.IO_EXCEPTION_CODE;

import static courses.web.ServletHelper.CONTENT_TYPE;
import static courses.web.ServletHelper.SHOW_DELETED_TASK;
import static courses.web.ServletHelper.SHOW_UPDATED_TASK;
import static courses.web.ServletHelper.SHOW_TASKS;
import static courses.web.ServletHelper.CHANGE_TASK_INFO;
import static courses.web.ServletHelper.getUrl;

import static java.lang.Integer.parseInt;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

import courses.builders.TaskBuilder;
import courses.entities.Task;
import courses.dao.DAOException;
import courses.services.TaskService;
import courses.validation.Validator;
import courses.validation.ValidateException;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletConfig;

import org.apache.log4j.Logger;

public class TaskServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(TaskServlet.class.getName());
    private TaskService taskService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        taskService = new TaskService();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType(CONTENT_TYPE);

        try {
            switch (getUrl(req)) {
                case SHOW_TASKS:
                    showTasksList(req, resp);
                    break;
                case SHOW_DELETED_TASK:
                    delete(req, resp);
                    break;
                case SHOW_UPDATED_TASK:
                    update(req, resp);
                    break;
                case CHANGE_TASK_INFO:
                    changeTask(req, resp);
                    break;
                default:
                    resourceError(req, resp);
                    break;
            }
        } catch (ValidateException ve) {
            logger.error(ve);
            resp.setStatus(SC_BAD_REQUEST);
            validateInfo(req, resp, ve.getMessage());
        } catch (DAOException e) {
            logger.error(e);
            resp.sendError(SC_INTERNAL_SERVER_ERROR, DAO_EXCEPTION_CODE);
        } catch (IOException ioe) {
            logger.error(ioe);
            resp.sendError(SC_INTERNAL_SERVER_ERROR, IO_EXCEPTION_CODE);
        }
    }

    private void resourceError(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("resourceError", "Resource not found");
        getServletContext().getRequestDispatcher("/jsp/errors/resourceError.jsp").forward(req, resp);
    }

    private void validateInfo(HttpServletRequest req, HttpServletResponse resp, String errorMessage) throws ServletException, IOException {
        req.setAttribute("invalidField", errorMessage);
        getServletContext().getRequestDispatcher("/jsp/errors/validateError.jsp").forward(req, resp);
    }

    private void showTasksList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, DAOException, IOException {
        req.setAttribute("tasks", taskService.findAll());
        getServletContext().getRequestDispatcher("/jsp/tasks/showTasksList.jsp").forward(req, resp);
    }

    private void changeTask(HttpServletRequest req, HttpServletResponse resp) throws ServletException, DAOException, IOException, ValidateException {
        Task task = TaskBuilder.build(req);
        if (req.getAttribute("error") != null) {
            getServletContext().getRequestDispatcher("/jsp/errors/xssError.jsp").forward(req, resp);
        } else {
            Validator.validateTask(task);
            int courseId = parseInt(req.getParameter("courseId"));
            if (courseId > 0) {
                task = taskService.addRelationships(task, courseId);
            }
            boolean result = taskService.create(task);

            req.setAttribute("changeTask", result);
            getServletContext().getRequestDispatcher("/jsp/tasks/createTaskResult.jsp").forward(req, resp);
        }
    }

    private void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, DAOException, IOException {
        String taskId = req.getParameter("deleteTaskId");
        int deletedTaskId = taskService.deleteTask(taskId);

        req.setAttribute("taskDel", deletedTaskId);
        getServletContext().getRequestDispatcher("/jsp/general/delete.jsp").forward(req, resp);
    }

    private void update(HttpServletRequest req, HttpServletResponse resp) throws ServletException, DAOException, IOException, ValidateException {
        Task task = TaskBuilder.build(req);
        if (req.getAttribute("error") != null) {
            getServletContext().getRequestDispatcher("/jsp/errors/xssError.jsp").forward(req, resp);
        } else {
            Validator.validateTask(task);
            int taskId = parseInt(req.getParameter("updateTaskId"));
            if (taskId > 0) {
                taskService.updateTask(task, taskId);
            }
            taskService.update(task);

            req.setAttribute("updatedTask", task);
            getServletContext().getRequestDispatcher("/jsp/general/update.jsp").forward(req, resp);
        }
    }

}
