package courses.web;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

class ServletHelper extends HttpServlet {

    static final String CONTENT_TYPE = "text/html; charset = utf-8";

    static final String SHOW_USERS = "/users/user";
    static final String SHOW_REGISTRATION = "/users/registration";
    static final String SHOW_DELETED_USER = "/users/delete";
    static final String SHOW_UPDATED_USER = "/users/update";
    static final String SHOW_LOGIN = "/users/login";

    static final String SHOW_MENU = "/courses";
    static final String SHOW_COURSES = "/courses/course";
    static final String SHOW_DELETED_COURSE = "/courses/delete";
    static final String SHOW_UPDATED_COURSE = "/courses/update";
    static final String CHANGE_COURSE_INFO = "/courses/changeCourse";

    static final String SHOW_MARKS = "/marks/mark";
    static final String SHOW_DELETED_MARK = "/marks/delete";
    static final String SHOW_UPDATED_MARK = "/marks/update";
    static final String CHANGE_MARK_INFO = "/marks/changeMark";

    static final String SHOW_TASKS = "/tasks/task";
    static final String SHOW_DELETED_TASK = "/tasks/delete";
    static final String SHOW_UPDATED_TASK = "/tasks/update";
    static final String CHANGE_TASK_INFO = "/tasks/changeTask";

    static void logout(HttpServletRequest req) {
        req.getSession().removeAttribute("user");
        req.getSession().invalidate();
    }

    static String getUrl(HttpServletRequest req) {
        return req.getRequestURI().substring(req.getContextPath().length());
    }
}
