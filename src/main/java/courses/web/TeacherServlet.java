package courses.web;

import static courses.dao.utils.ErrorCodeHelper.DAO_EXCEPTION_CODE;
import static courses.dao.utils.ErrorCodeHelper.IO_EXCEPTION_CODE;

import static courses.web.ServletHelper.CONTENT_TYPE;

import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

import courses.dao.DAOException;
import courses.services.TeacherService;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletConfig;

import org.apache.log4j.Logger;

public class TeacherServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(TeacherServlet.class.getName());
    private TeacherService teacherService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        teacherService = new TeacherService();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType(CONTENT_TYPE);

        try {
            showTeachersList(req, resp);
        } catch (DAOException e) {
            logger.error(e);
            resp.sendError(SC_INTERNAL_SERVER_ERROR, DAO_EXCEPTION_CODE);
        } catch (IOException ioe) {
            logger.error(ioe);
            resp.sendError(SC_INTERNAL_SERVER_ERROR, IO_EXCEPTION_CODE);
        }
    }

    private void showTeachersList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, DAOException {
        req.setAttribute("teachers", teacherService.findAll());
        getServletContext().getRequestDispatcher("/jsp/teachers/showTeachersList.jsp").forward(req, resp);
    }

}
