package courses.web;

import static courses.dao.utils.ErrorCodeHelper.DAO_EXCEPTION_CODE;
import static courses.dao.utils.ErrorCodeHelper.IO_EXCEPTION_CODE;

import static courses.web.ServletHelper.CONTENT_TYPE;
import static courses.web.ServletHelper.SHOW_USERS;
import static courses.web.ServletHelper.SHOW_REGISTRATION;
import static courses.web.ServletHelper.SHOW_DELETED_USER;
import static courses.web.ServletHelper.SHOW_UPDATED_USER;
import static courses.web.ServletHelper.SHOW_LOGIN;
import static courses.web.ServletHelper.getUrl;

import static java.lang.Integer.parseInt;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

import courses.builders.UserBuilder;
import courses.entities.UserType;
import courses.entities.User;
import courses.dao.DAOException;
import courses.services.UserService;
import courses.validation.Validator;
import courses.validation.ValidateException;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletConfig;

import org.apache.log4j.Logger;

public class UserServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(UserServlet.class.getName());
    private UserService userService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        userService = new UserService();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType(CONTENT_TYPE);

        try {
            switch (getUrl(req)) {
                case SHOW_USERS:
                    showUsersList(req, resp);
                    break;
                case SHOW_REGISTRATION:
                    registration(req, resp);
                    break;
                case SHOW_DELETED_USER:
                    delete(req, resp);
                    break;
                case SHOW_UPDATED_USER:
                    update(req, resp);
                    break;
                case SHOW_LOGIN:
                    login(req, resp);
                    break;
                default:
                    resourceError(req, resp);
                    break;
            }
        } catch (ValidateException ve) {
            logger.error(ve);
            resp.setStatus(SC_BAD_REQUEST);
            validateInfo(req, resp, ve.getMessage());
        } catch (DAOException e) {
            logger.error(e);
            resp.sendError(SC_INTERNAL_SERVER_ERROR, DAO_EXCEPTION_CODE);
        } catch (IOException ioe) {
            logger.error(ioe);
            resp.sendError(SC_INTERNAL_SERVER_ERROR, IO_EXCEPTION_CODE);
        }

    }

    private void resourceError(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("resourceError", "Resource not found");
        getServletContext().getRequestDispatcher("/jsp/errors/resourceError.jsp").forward(req, resp);
    }

    private void validateInfo(HttpServletRequest req, HttpServletResponse resp, String errorMessage) throws ServletException, IOException {
        req.setAttribute("invalidField", errorMessage);
        getServletContext().getRequestDispatcher("/jsp/errors/validateError.jsp").forward(req, resp);
    }

    private void showUsersList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, DAOException, IOException {
        req.setAttribute("users", userService.getUsersDto());
        getServletContext().getRequestDispatcher("/jsp/users/showUsersList.jsp").forward(req, resp);
    }

    private void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, DAOException, IOException {
        String userId = req.getParameter("deleteUserId");
        int deletedUserId = userService.deleteUser(userId);

        req.setAttribute("userDel", deletedUserId);
        getServletContext().getRequestDispatcher("/jsp/general/delete.jsp").forward(req, resp);
    }

    private void update(HttpServletRequest req, HttpServletResponse resp) throws ServletException, DAOException, IOException, ValidateException {

        int userId = parseInt(req.getParameter("updateUserId"));

        User user = null;
        user = getUserFromRequest(req, user);
        user = userService.setPassAndId(user, userId);

        if (req.getAttribute("error") != null) {
            getServletContext().getRequestDispatcher("/jsp/errors/xssError.jsp").forward(req, resp);
        } else {
            if (user != null) {
                Validator.validateUser(user);
                userService.update(user);
                req.setAttribute("updatedUser", user);
            }

            getServletContext().getRequestDispatcher("/jsp/general/update.jsp").forward(req, resp);
        }
    }

    private void registration(HttpServletRequest req, HttpServletResponse resp) throws ServletException, DAOException, IOException, ValidateException {
        User user = null;
        user = getUserFromRequest(req, user);

        if (req.getAttribute("error") != null) {
            getServletContext().getRequestDispatcher("/jsp/errors/xssError.jsp").forward(req, resp);
        } else {
            if (user != null) {
                Validator.validateUser(user);
                boolean result = userService.create(user);
                req.setAttribute("registerUser", result);
            }

            getServletContext().getRequestDispatcher("/jsp/users/registrationResult.jsp").forward(req, resp);
        }
    }

    private void login(HttpServletRequest req, HttpServletResponse resp) throws ServletException, DAOException, IOException {
        String email = req.getParameter("userEmail");
        String password = req.getParameter("userPassword");
        boolean result = userService.authenticateUser(email, password);
        if (result) {
            User user = userService.getUserByEmail(email);
            req.getSession().setAttribute("user", user);
            getServletContext().getRequestDispatcher("/courses/menu").forward(req, resp);
        } else {
            getServletContext().getRequestDispatcher("/courses/error").forward(req, resp);
        }
    }

    private User getUserFromRequest(HttpServletRequest req, User user) throws ValidateException {
        String userType = req.getParameter("userType");
        UserType userTypeEnum = UserType.fromCode(userType);
        switch (userTypeEnum) {
            case TEACHER:
                user = UserBuilder.buildTeacher(req);
                break;
            case STUDENT:
                user = UserBuilder.buildStudent(req);
                break;
            default:
                throw new ValidateException("user has no correct type");
        }
        return user;
    }

}
