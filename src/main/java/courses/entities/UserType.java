package courses.entities;

public enum UserType {
    TEACHER("TEACHER"),
    STUDENT("STUDENT");

    private String type;

    UserType(final String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static UserType fromCode(final String code) {
        for (final UserType type : values()) {
            if (type.name().equalsIgnoreCase(code)) {
                return type;
            }
        }
        return null;
    }
}

