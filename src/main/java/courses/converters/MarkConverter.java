package courses.converters;

import courses.entities.Mark;
import courses.entities.Student;
import courses.entities.Task;
import courses.model.MarkDto;

public class MarkConverter {

    public static MarkDto toMarkDto(Mark mark, Student student, Task task) {
        MarkDto dto = new MarkDto();
        dto.setMarkId(mark.getMarkId());
        dto.setComment(mark.getComment());
        dto.setMark(mark.getMark());
        dto.setStudentName(student.getName());
        dto.setStudentSurname(student.getSurname());
        dto.setNameOfTask(task.getNameOfTask());

        return dto;
    }
}
