package courses.converters;

import courses.entities.Teacher;
import courses.entities.User;
import courses.model.UserDto;

public class UserConverter {

    public static UserDto toUserDto(User user, Teacher teacher) {
        UserDto dto = new UserDto();
        dto.setUserId(user.getUserId());
        dto.setEmail(user.getEmail());
        dto.setName(user.getName());
        dto.setSurname(user.getSurname());
        dto.setAddress(user.getAddress());
        dto.setPhoneNum(user.getPhoneNum());
        dto.setType(user.getType());
        if (teacher != null) {
            dto.setSpecialization(teacher.getSpecialization());
        }

        return dto;
    }
}
