package courses.builders;

import static java.lang.Integer.parseInt;

import courses.entities.Course;
import courses.validation.Validator;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.text.StringEscapeUtils;

public class CourseBuilder {

    public static Course build(HttpServletRequest req) {
        Course course = new Course();
        Validator.validateCourseXss(req);
        course.setType(StringEscapeUtils.escapeHtml4(req.getParameter("type")));
        course.setTitle(StringEscapeUtils.escapeHtml4(req.getParameter("title")));
        course.setNumOfHours(parseInt(StringEscapeUtils.escapeHtml4(req.getParameter("numOfHours"))));
        course.setDescription(StringEscapeUtils.escapeHtml4(req.getParameter("description")));

        return course;
    }
}
