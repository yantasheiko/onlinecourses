package courses.builders;

import static java.lang.Integer.parseInt;

import courses.entities.Mark;
import courses.validation.Validator;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.text.StringEscapeUtils;

public class MarkBuilder {
    public static Mark build(HttpServletRequest req) {
        Mark mark = new Mark();
        Validator.validateMarkXss(req);
        mark.setComment(StringEscapeUtils.escapeHtml4(req.getParameter("comment")));
        mark.setMark(parseInt(StringEscapeUtils.escapeHtml4(req.getParameter("mark"))));

        return mark;
    }
}
