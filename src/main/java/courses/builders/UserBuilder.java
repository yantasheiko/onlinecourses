package courses.builders;

import courses.entities.Student;
import courses.entities.Teacher;
import courses.entities.UserType;
import courses.validation.Validator;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.text.StringEscapeUtils;

public class UserBuilder {

    public static Teacher buildTeacher(HttpServletRequest req) {
        Teacher user = new Teacher();
        Validator.validateUserXss(req);
        user.setEmail(StringEscapeUtils.escapeHtml4(req.getParameter("email")));
        user.setPassword(req.getParameter("password"));
        user.setName(StringEscapeUtils.escapeHtml4(req.getParameter("name")));
        user.setSurname(StringEscapeUtils.escapeHtml4(req.getParameter("surname")));
        user.setAddress(StringEscapeUtils.escapeHtml4(req.getParameter("address")));
        user.setPhoneNum(StringEscapeUtils.escapeHtml4(req.getParameter("phoneNum")));
        user.setType(UserType.valueOf(StringEscapeUtils.escapeHtml4(req.getParameter("userType"))));
        user.setSpecialization(StringEscapeUtils.escapeHtml4(req.getParameter("specialization")));

        return user;
    }

    public static Student buildStudent(HttpServletRequest req) {
        Student user = new Student();
        Validator.validateUserXss(req);
        user.setEmail(StringEscapeUtils.escapeHtml4(req.getParameter("email")));
        user.setPassword(req.getParameter("password"));
        user.setName(StringEscapeUtils.escapeHtml4(req.getParameter("name")));
        user.setSurname(StringEscapeUtils.escapeHtml4(req.getParameter("surname")));
        user.setAddress(StringEscapeUtils.escapeHtml4(req.getParameter("address")));
        user.setPhoneNum(StringEscapeUtils.escapeHtml4(req.getParameter("phoneNum")));
        user.setType(UserType.valueOf(StringEscapeUtils.escapeHtml4(req.getParameter("userType"))));

        return user;
    }
}
