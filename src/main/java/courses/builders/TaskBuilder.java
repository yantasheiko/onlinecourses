package courses.builders;

import courses.entities.Task;
import courses.validation.Validator;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.text.StringEscapeUtils;

public class TaskBuilder {

    public static Task build(HttpServletRequest req) {
        Task task = new Task();
        Validator.validateTaskXss(req);
        task.setNameOfTask(StringEscapeUtils.escapeHtml4(req.getParameter("nameOfTask")));
        task.setDescription(StringEscapeUtils.escapeHtml4(req.getParameter("description")));

        return task;
    }

}
