package courses.dao;

public interface MarkDao {

    /**
     * Get query string
     *
     * @return string mark query by comment
     */
    String getQueryByComment();
}
