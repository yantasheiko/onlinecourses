package courses.dao.impl;

import courses.entities.Teacher;

public class TeacherDaoImpl extends BaseDaoImpl<Teacher> {
    public TeacherDaoImpl() {
        super(Teacher.class);
    }
}
