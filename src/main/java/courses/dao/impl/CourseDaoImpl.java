package courses.dao.impl;

import courses.dao.CourseDao;
import courses.entities.Course;

public class CourseDaoImpl extends BaseDaoImpl<Course> implements CourseDao {

    public CourseDaoImpl() {
        super(Course.class);
    }

    private static final String QUERY_BY_FIELD = "from Course where title='%s'";

    public String getQueryByTitle() {
        return QUERY_BY_FIELD;
    }

    public boolean isExists(Course entity) {
         return isExists(entity.getTitle(), getQueryByTitle());
    }

}
