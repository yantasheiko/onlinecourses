package courses.dao.impl;

import courses.dao.UserDao;
import courses.entities.User;
import org.hibernate.Session;

public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {

    public UserDaoImpl() {
        super(User.class);
    }

    private static final String QUERY_BY_FIELD = "from User where email='%s'";

    public User getUserByEmail(String email, Session session) {
        return (User) session.createQuery(String.format(QUERY_BY_FIELD, email)).uniqueResult();
    }

    public String getQueryByEmail() {
        return QUERY_BY_FIELD;
    }

    public boolean isExists(User entity) {
        return isExists(entity.getEmail(), getQueryByEmail());
    }
}
