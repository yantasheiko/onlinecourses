package courses.dao.impl;

import courses.dao.GenericDao;
import courses.dao.utils.HibernateUtil;

import java.util.List;

import org.hibernate.Session;

public class BaseDaoImpl<T> implements GenericDao<T> {

    private static Session currentSession;
    private Class<T> typeParameterClass;

    BaseDaoImpl(Class<T> typeParameterClass) {
        this.typeParameterClass = typeParameterClass;
    }

    public BaseDaoImpl() {
    }

    public void openCurrentSession() {
        currentSession = HibernateUtil.getSessionFactory().openSession();
    }

    public void closeCurrentSession() {
        currentSession.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    public T findById(Integer id) {
        return (T) getCurrentSession().get(typeParameterClass, id);
    }

    public List<T> findAll() {
        return getCurrentSession().createQuery(getEntityQuery()).list();
    }

    public void update(T entity) {
        getCurrentSession().update(entity);
    }

    public void delete(T entity) {
        getCurrentSession().delete(entity);
    }

    public void add(T entity) {
        getCurrentSession().saveOrUpdate(entity);
    }

    public boolean isExists(String entityUniqueField, String queryByField) {
        T cs = (T) getCurrentSession().createQuery(String.format(queryByField, entityUniqueField)).uniqueResult();
        return cs != null;
    }

    private String getEntityQuery() {
        String entityQuery = null;
        if (typeParameterClass != null) {
            entityQuery = "from " + typeParameterClass.getSimpleName();
        }
        return entityQuery;
    }

}
