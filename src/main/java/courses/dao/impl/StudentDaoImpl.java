package courses.dao.impl;

import courses.entities.Student;

public class StudentDaoImpl extends BaseDaoImpl<Student> {
    public StudentDaoImpl() {
        super(Student.class);
    }
}
