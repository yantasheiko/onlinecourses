package courses.dao.impl;

import courses.dao.TaskDao;
import courses.entities.Task;

public class TaskDaoImpl extends BaseDaoImpl<Task> implements TaskDao {

    public TaskDaoImpl() {
        super(Task.class);
    }

    private static final String QUERY_BY_FIELD = "from Task where nameOfTask='%s'";

    public String getQueryByTaskName() {
        return QUERY_BY_FIELD;
    }

    public boolean isExists(Task entity) {
        return isExists(entity.getNameOfTask(), getQueryByTaskName());
    }

}
