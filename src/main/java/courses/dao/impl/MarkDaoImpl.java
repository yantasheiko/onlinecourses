package courses.dao.impl;

import courses.dao.MarkDao;
import courses.entities.Mark;

public class MarkDaoImpl extends BaseDaoImpl<Mark> implements MarkDao {

    public MarkDaoImpl() {
        super(Mark.class);
    }

    private static final String QUERY_BY_FIELD = "from Mark where comment='%s'";

    public String getQueryByComment() {
        return QUERY_BY_FIELD;
    }

    public boolean isExists(Mark entity) {
        return isExists(entity.getComment(), getQueryByComment());
    }
}
