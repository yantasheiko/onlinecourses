package courses.dao;

public interface CourseDao {
    /**
     * Get query string
     *
     * @return string course query by title
     */
    String getQueryByTitle();
}
