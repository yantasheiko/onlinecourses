package courses.dao;

import courses.entities.User;
import org.hibernate.Session;

public interface UserDao {

    /**
     * Get entity by email from DB
     *
     * @param email - selected user email
     * @param session - Hibernate session
     * @return user entity which matches
     * this email
     */
    User getUserByEmail(String email, Session session);

    /**
     * Get query string
     *
     * @return string user query by email
     */
    String getQueryByEmail();

}