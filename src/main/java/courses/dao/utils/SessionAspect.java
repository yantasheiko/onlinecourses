package courses.dao.utils;

import courses.dao.DAOException;
import courses.dao.impl.BaseDaoImpl;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.AfterThrowing;

@Aspect
public class SessionAspect {
    private BaseDaoImpl dao = new BaseDaoImpl();
    private static final String EXCEPTION_MESSAGE = "Connection error to the DB :";

    @Pointcut("execution(* courses.services.*.findById(..)) ||" +
            " execution(* courses.services.*.findAll(..)) ||" +
            "execution(* courses.services.*.isExists(..)) ")
    protected void find() {
    }

    @Before("find()")
    public void findJoinPoint() {
        dao.openCurrentSession();
    }

    @After("find()")
    public void findAfter() {
        dao.closeCurrentSession();
    }

    @AfterThrowing(pointcut = "find()", throwing = "e")
    public void findAfterThrowing(JoinPoint joinPoint, Throwable e) throws DAOException {
        throw new DAOException(EXCEPTION_MESSAGE, e);
    }

}
