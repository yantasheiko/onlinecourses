package courses.dao.utils;

import courses.dao.DAOException;
import courses.dao.impl.BaseDaoImpl;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.AfterReturning;
import org.hibernate.Transaction;

@Aspect
public class TransactionAspect {

    private static final Logger logger = Logger.getLogger(TransactionAspect.class.getName());
    private Transaction currentTransaction;
    private BaseDaoImpl dao = new BaseDaoImpl();
    private static final String EXCEPTION_MESSAGE = "Connection error to the DB :";

    @Pointcut("execution(* courses.services.*.delete(..)) " +
            "|| execution(* courses.services.*.update(..)) " +
            "|| execution(* courses.services.*.add(..)) " +
            "|| execution(* courses.services.*.getUserByEmail(..)) ")
    protected void transactionsPoint() {
    }

    @Before("transactionsPoint()")
    public void transactionsJoinPoint() {
        dao.openCurrentSession();
        currentTransaction = dao.getCurrentSession().beginTransaction();
    }

    @AfterReturning("transactionsPoint()")
    public void transactionsAfterReturn() {
        currentTransaction.commit();
    }

    @After("transactionsPoint()")
    public void transactionsAfter() {
        dao.closeCurrentSession();
    }

    @AfterThrowing(pointcut = "transactionsPoint()", throwing = "e")
    public void transactionsAfterThrowing(JoinPoint joinPoint, Throwable e) throws DAOException {
        if (currentTransaction != null) {
            currentTransaction.rollback();
        }
        logger.error("Exception was thrown");
        throw new DAOException(EXCEPTION_MESSAGE, e);
    }

}


