package courses.dao.utils;

public class ErrorCodeHelper {

    /**
     * DAO error code that show that was database connection error
     */
    public static final String DAO_EXCEPTION_CODE = "error code 505";
    /**
     * IO error code that show that was Input/Output error
     */
    public static final String IO_EXCEPTION_CODE = "error code 510";

}
