package courses.dao;

public interface TaskDao {

    /**
     * Get query string
     *
     * @return string task query by name
     * of task
     */
    String getQueryByTaskName();

}
