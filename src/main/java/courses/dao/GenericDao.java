package courses.dao;

import java.util.List;

public interface GenericDao<T> {

    /**
     * Delete object record from DB
     *
     * @param entity - selected entity
     */
    void delete(T entity) throws DAOException;

    /**
     * Write new object record in DB
     *
     * @param entity - selected entity
     */
    void add(T entity) throws DAOException;

    /**
     * Update object record in DB
     *
     * @param entity - selected entity
     */
    void update(T entity) throws DAOException;

    /**
     *
     * @return result if entity exists in DB
     *
     * @param entityUniqueField - unique field(e.g. email)
     * @param queryByField - unique entity(e.g. Course)
     */
    boolean isExists(String entityUniqueField, String queryByField) throws DAOException;

    /**
     * Get all object records from DB
     *
     * @return list of all existing objects
     * in table
     */
    List<T> findAll() throws DAOException;

    /**
     * Get object record from DB by id
     *
     * @param id - identifier of entity
     * @return object of appropriate this id
     */
    T findById(Integer id) throws DAOException;

}