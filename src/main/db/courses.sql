DROP DATABASE `onlineCourses`;
CREATE SCHEMA IF NOT EXISTS `onlineCourses` DEFAULT CHARACTER SET utf8;
USE `onlineCourses`;

CREATE TABLE IF NOT EXISTS `onlineCourses`.`User` (
    `userId` INT NOT NULL AUTO_INCREMENT,
    `email` VARCHAR(45),
    `password` VARCHAR(45),
    `name` VARCHAR(45) NOT NULL,
    `surname` VARCHAR(45) NOT NULL,
    `address` VARCHAR(45),
    `phoneNum` VARCHAR(45),
    `type` VARCHAR(45),
    `admin` TINYINT(1),
    PRIMARY KEY (`userId`)
);

CREATE TABLE IF NOT EXISTS `onlineCourses`.`Teacher` (
    `teacherId` INT NOT NULL,
    `specialization` VARCHAR(45),
    PRIMARY KEY (`teacherId`),
    KEY `fk_new_teacher` (`teacherId`),
    CONSTRAINT `fk_new_teacher` FOREIGN KEY (`teacherId`) REFERENCES `user` (`userId`)
);

CREATE TABLE IF NOT EXISTS `onlineCourses`.`Course` (
    `courseId` INT NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(45) NOT NULL,
    `title` VARCHAR(45) NOT NULL,
    `numOfHours` INT NOT NULL,
    `description` VARCHAR(90),
    PRIMARY KEY (`courseId`)
);
   
CREATE TABLE IF NOT EXISTS `onlineCourses`.`Student` (
    `studentId` INT NOT NULL,
    PRIMARY KEY (`studentId`),
    KEY `fk_new_student` (`studentId`),
    CONSTRAINT `fk_new_student` FOREIGN KEY (`studentId`) REFERENCES `user` (`userId`)
);

CREATE TABLE IF NOT EXISTS `onlineCourses`.`Task` (
    `taskId` INT NOT NULL AUTO_INCREMENT,
    `nameOfTask` VARCHAR(45) NOT NULL,
    `description` TEXT,
    `courseId` INT NOT NULL,
    PRIMARY KEY (`taskId`),
    KEY `fk_course` (`courseId`),
    CONSTRAINT `fk_course` FOREIGN KEY (`courseId`) REFERENCES `course` (`courseId`)
);

CREATE TABLE IF NOT EXISTS `onlineCourses`.`Mark` (
    `markId` INT NOT NULL AUTO_INCREMENT,
    `comment` VARCHAR(200) NOT NULL,
    `mark` INT NOT NULL,
    `studentId` INT NOT NULL,
    `taskId` INT NOT NULL,
    PRIMARY KEY (`markId`),
    KEY `fk_student` (`studentId`),
    CONSTRAINT `fk_student` FOREIGN KEY (`studentId`) REFERENCES `student` (`studentId`),
    KEY `fk_task` (`taskId`),
    CONSTRAINT `fk_task` FOREIGN KEY (`taskId`) REFERENCES `task` (`taskId`)
);

CREATE TABLE IF NOT EXISTS `onlineCourses`.`course_teacher` (
    `courseId` INT NOT NULL,
    `teacherId` INT NOT NULL,
    CONSTRAINT `fk_course_id` FOREIGN KEY (`courseId`) REFERENCES `course` (`courseId`),
    CONSTRAINT `fk_teacher_id` FOREIGN KEY (`teacherId`) REFERENCES `teacher` (`teacherId`)
);

INSERT INTO `onlineCourses`.`User` (`email`, `password`, `name`, `surname`, `address`, `phoneNum`, `admin`)
VALUES ('yantasheiko@gmail.com', 'dager411', 'Yan', 'Tasheika', 'Minsk, st. Alexandrova, 11', '+375292091914', '1');
