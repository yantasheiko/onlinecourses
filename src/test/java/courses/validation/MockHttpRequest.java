package courses.validation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.HashMap;

class MockHttpRequest extends HttpServletRequestWrapper {

    private HashMap<String, String> params = new HashMap<>();

    private HashMap<String, Object> attributes = new HashMap<>();

    MockHttpRequest(final HttpServletRequest request) {
        super(request);
    }

    public String getParameter(String name) {
        if (params.get(name) != null) {
            return params.get(name);
        }

        HttpServletRequest req = (HttpServletRequest) super.getRequest();
        return req.getParameter(name);
    }

    void addParameter(String name, String value) {
        params.put(name, value);
    }

    public void setAttribute(String name, Object value) {
        attributes.put(name, value);
    }

    public String getAttribute(String name) {
        if (attributes.get(name) != null) {
            return attributes.get(name).toString();
        }

        HttpServletRequest req = (HttpServletRequest) super.getRequest();
        return req.getAttribute(name).toString();
    }
}
