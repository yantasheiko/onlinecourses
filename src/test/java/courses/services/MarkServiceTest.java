package courses.services;

import static courses.services.TestDataHelper.getMark;
import static courses.services.TestDataHelper.getMarksList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import static org.testng.Assert.assertEquals;

import courses.dao.impl.MarkDaoImpl;
import courses.entities.Mark;

import java.util.List;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

@RunWith(MockitoJUnitRunner.class)
public class MarkServiceTest {

    @Mock
    private MarkDaoImpl dao;

    @InjectMocks
    private MarkService markService;

    @BeforeSuite
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAll_ReturnMarks() {
        List<Mark> list = getMarksList();
        when(dao.findAll()).thenReturn(list);

        assertEquals(markService.findAll(), list);
    }

    @Test
    public void testFindById_ValidId_ReturnMark() {
        Mark mark = getMark();
        when(dao.findById(1)).thenReturn(mark);

        assertEquals(markService.findById(1), mark);
    }

    @Test
    public void testUpdate_ValidMark_VerifyInvocationsNumber() {
        Mark mark = getMark();
        when(dao.findById(1)).thenReturn(mark);
        markService.update(mark);

        verify(dao, times(1)).update(mark);
    }

    @Test
    public void testAdd_ValidMark_VerifyInvocationsNumber() {
        Mark mark = getMark();
        markService.add(mark);

        verify(dao, times(1)).add(mark);
    }

    @Test
    public void testDelete_ValidMark_VerifyInvocationsNumber() {
        Mark mark = getMark();
        when(dao.findById(4)).thenReturn(mark);
        markService.delete(mark);

        verify(dao, times(1)).delete(mark);
    }
}
