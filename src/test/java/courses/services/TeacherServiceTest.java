package courses.services;

import static courses.services.TestDataHelper.getTeacher;
import static courses.services.TestDataHelper.getTeachersList;
import static org.mockito.Mockito.when;

import static org.testng.Assert.assertEquals;

import courses.dao.impl.TeacherDaoImpl;
import courses.entities.Teacher;

import java.util.List;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

@RunWith(MockitoJUnitRunner.class)
public class TeacherServiceTest {

    @Mock
    private TeacherDaoImpl dao;

    @InjectMocks
    private TeacherService teacherService;

    @BeforeSuite
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAll_ReturnTeachers() {
        List<Teacher> list = getTeachersList();
        when(dao.findAll()).thenReturn(list);

        assertEquals(teacherService.findAll(), list);
    }

    @Test
    public void testFindById_ValidId_ReturnTeacher() {
        Teacher teacher = getTeacher();
        when(dao.findById(1)).thenReturn(teacher);

        assertEquals(teacherService.findById(1), teacher);
    }
}
