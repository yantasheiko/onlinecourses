package courses.services;

import static courses.services.TestDataHelper.getUser;
import static courses.services.TestDataHelper.getUsersList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import courses.dao.impl.UserDaoImpl;
import courses.entities.User;

import java.util.List;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private UserDaoImpl dao;

    @InjectMocks
    private UserService userService;

    @BeforeSuite
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAll_ReturnUsers() {
        List<User> list = getUsersList();
        when(dao.findAll()).thenReturn(list);

        assertEquals(userService.findAll(), list);
    }

    @Test
    public void testFindById_ValidId_ReturnUser() {
        User user = getUser();
        when(dao.findById(3)).thenReturn(user);

        assertEquals(userService.findById(3), user);
    }

    @Test
    public void testUpdate_ValidUser_VerifyInvocationsNumber() {
        User user = getUser();
        when(dao.findById(3)).thenReturn(user);
        userService.update(user);

        verify(dao, times(1)).update(user);
    }

    @Test
    public void testAdd_ValidUser_VerifyInvocationsNumber() {
        User user = getUser();
        userService.add(user);

        verify(dao, times(1)).add(user);
    }

    @Test
    public void testDelete_ValidUser_VerifyInvocationsNumber() {
        User user = getUser();
        when(dao.findById(3)).thenReturn(user);
        userService.delete(user);

        verify(dao, times(1)).delete(user);
    }

    @Test
    public void testIsExists_ValidUser_ReturnCondition() {
        when(dao.isExists(any())).thenReturn(true);

        assertTrue(userService.isExists(any()));
    }

    @Test
    public void testGetUserByEmail_ValidUserEmail_ReturnUser() {
        User user = getUser();
        when(dao.getUserByEmail(user.getEmail(), dao.getCurrentSession())).thenReturn(user);

        assertEquals(userService.getUserByEmail(user.getEmail()), user);
    }
}
