package courses.services;

import static courses.services.TestDataHelper.getStudent;
import static courses.services.TestDataHelper.getStudentsList;
import static org.mockito.Mockito.when;

import static org.testng.Assert.assertEquals;

import courses.dao.impl.StudentDaoImpl;
import courses.entities.Student;

import java.util.List;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

@RunWith(MockitoJUnitRunner.class)
public class StudentServiceTest {

    @Mock
    private StudentDaoImpl dao;

    @InjectMocks
    private StudentService studentService;

    @BeforeSuite
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAll_ReturnStudents() {
        List<Student> list = getStudentsList();
        when(dao.findAll()).thenReturn(list);

        assertEquals(studentService.findAll(), list);
    }

    @Test
    public void testFindById_ValidId_ReturnStudent() {
        Student student = getStudent();
        when(dao.findById(1)).thenReturn(student);

        assertEquals(studentService.findById(1), student);
    }
}
