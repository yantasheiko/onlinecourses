package courses.services;

import static courses.services.TestDataHelper.getTask;
import static courses.services.TestDataHelper.getTasksList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import static org.testng.Assert.assertEquals;

import courses.dao.impl.TaskDaoImpl;
import courses.entities.Task;

import java.util.List;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

@RunWith(MockitoJUnitRunner.class)
public class TaskServiceTest {

    @Mock
    private TaskDaoImpl dao;

    @InjectMocks
    private TaskService taskService;

    @BeforeSuite
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAll_ReturnTasks() {
        List<Task> list = getTasksList();
        when(dao.findAll()).thenReturn(list);

        assertEquals(taskService.findAll(), list);
    }

    @Test
    public void testFindById_ValidId_ReturnTask() {
        Task task = getTask();
        when(dao.findById(1)).thenReturn(task);

        assertEquals(taskService.findById(1), task);
    }

    @Test
    public void testUpdate_ValidTask_VerifyInvocationsNumber() {
        Task task = getTask();
        when(dao.findById(1)).thenReturn(task);
        taskService.update(task);

        verify(dao, times(1)).update(task);
    }

    @Test
    public void testAdd_ValidTask_VerifyInvocationsNumber() {
        Task task = getTask();
        taskService.add(task);

        verify(dao, times(1)).add(task);
    }

    @Test
    public void testDelete_ValidTask_VerifyInvocationsNumber() {
        Task task = getTask();
        when(dao.findById(1)).thenReturn(task);
        taskService.delete(task);

        verify(dao, times(1)).delete(task);
    }
}
