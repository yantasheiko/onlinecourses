package courses.converters;

import static courses.converters.TestDataHelper.getMark;
import static courses.converters.TestDataHelper.getStudentForMark;
import static courses.converters.TestDataHelper.getTaskForMark;
import static courses.converters.MarkConverter.toMarkDto;

import courses.entities.Mark;
import courses.entities.Student;
import courses.entities.Task;
import courses.model.MarkDto;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class MarkConverterTest {

    @BeforeSuite
    public void setUp() {
    }

    @Test
    public void testToMarkDto() {
        Mark mark = getMark();
        Student student = getStudentForMark();
        Task task = getTaskForMark();
        MarkDto dto = toMarkDto(mark, student, task);

        Assert.assertEquals(dto.getMarkId(), mark.getMarkId());
        Assert.assertEquals(dto.getComment(), mark.getComment());
        Assert.assertEquals(dto.getMark(), mark.getMark());
        Assert.assertEquals(dto.getStudentName(), student.getName());
        Assert.assertEquals(dto.getStudentSurname(), student.getSurname());
        Assert.assertEquals(dto.getNameOfTask(), task.getNameOfTask());
    }
}
