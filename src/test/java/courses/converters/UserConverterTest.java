package courses.converters;

import static courses.converters.TestDataHelper.getUser;
import static courses.converters.TestDataHelper.getTeacherForUser;
import static courses.converters.UserConverter.toUserDto;

import courses.entities.Teacher;
import courses.entities.User;
import courses.model.UserDto;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class UserConverterTest {

    @BeforeSuite
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testToUserDto() {
        User user = getUser();
        Teacher teacher = getTeacherForUser();
        UserDto dto = toUserDto(user, teacher);

        Assert.assertEquals(dto.getEmail(), user.getEmail());
        Assert.assertEquals(dto.getName(), user.getName());
        Assert.assertEquals(dto.getSurname(), user.getSurname());
        Assert.assertEquals(dto.getSpecialization(), teacher.getSpecialization());
    }
}
